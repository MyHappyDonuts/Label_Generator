"""
\file LabelGenerator.py
\brief Short brief for this module.
\author Javier Morales

\details Description for this module.
"""

###############
## Import
###############
## Native libraries

## External modules
from fpdf import FPDF

###############
## Constants
###############
PAGE_WIDTH = 210
PAGE_HEIGHT = 297

LABEL_WIDTH = 75
LABEL_HEIGHT = 110

IMAGE_WIDTH = 18

LEFT_MARGIN = 5
HORIZONTAL_SPACE = 5

###############
## Code
###############
class Label():

    def __init__(self, type, ref, mfr_ref, manufacturer, description, value, image) -> None:
        
        self.type = type = type
        self.ref = ref
        self.mfr_ref = mfr_ref
        self.manufacturer = manufacturer
        self.description = description
        self.value = value
        self.image = image

class PDF(FPDF):

    def __init__(self, orientation) -> None:
        super().__init__(orientation)

        self.page_index = 0
        self.label_number = 0
        self.add_page()

    def _label_x(self,relative_x):
        absolute_x = 0
        match self.page_index:
            case 0 | 2:
                absolute_x = (relative_x + 10)
            case 1 | 3:
                absolute_x = (relative_x + 30 + LABEL_WIDTH)
        return absolute_x
    
    def _label_y(self,relative_y):
        absolute_y = 0
        match self.page_index:
            case 0 | 1:
                absolute_y = (relative_y + 10)
            case 2 | 3:
                absolute_y = (relative_y + 30 + LABEL_HEIGHT)
        return absolute_y        

    def _set_border(self, x, y, width, height):
        self.rect(x, y, width, height)

    def add_label(self, label):
        if (self.page_index >= 4):
            self.page_index = 0
            self.add_page()

        self._set_border(self._label_x(0), self._label_y(0), LABEL_WIDTH, LABEL_HEIGHT)
        self._set_type(label)
        self.line(self._label_x(0),self._label_y(20), self._label_x(LABEL_WIDTH),self._label_y(20))
        self._set_reference(label)
        self._set_mrf_reference(label)
        self._set_description(label)
        self._set_manufacturer(label)
        self._set_value(label)
        self._set_image(label)

        self.page_index = self.page_index + 1
        self.label_number = self.label_number + 1
        print("Label " + str(self.label_number) + " added")

    def _set_type(self, label):
        self.set_xy(self._label_x(LEFT_MARGIN), self._label_y(10))
        self.set_font('Arial', 'B', 16)
        self.cell(w=20, h=0, align='L', txt=label.type, border=0, ln=0)

    def _set_reference(self, label):
        self.set_xy(self._label_x(LEFT_MARGIN), self._label_y(24))
        self.set_font('Arial', '', 10)
        self.cell(w=20, h=0, align='L', txt="Reference:", border=0, ln=0)

        self.set_xy(self._label_x(LEFT_MARGIN), self._label_y(28))
        self.set_font('Arial', '', 10)
        self.cell(w=20, h=0, align='L', txt=label.ref, border=0, ln=0)

    def _set_mrf_reference(self, label):
        self.set_xy(self._label_x(LEFT_MARGIN), self._label_y(38))
        self.set_font('Arial', '', 10)
        self.cell(w=0, h=0, align='L', txt="Mfr. Number:", border=0)

        self.set_xy(self._label_x(LEFT_MARGIN), self._label_y(41))
        self.set_font('Arial', '', 14)
        # self.cell(w=0, h=0, align='L', txt=label.mfr_ref, border=0)
        self.multi_cell(w=(LABEL_WIDTH - 2*LEFT_MARGIN), h=5, align='L', txt=label.mfr_ref, border=0)
        self.ln

    def _set_manufacturer(self, label):
        self.set_xy(self._label_x(LEFT_MARGIN), self._label_y(55))
        self.set_font('Arial', '', 10)
        self.cell(w=0, h=0, align='L', txt="Manufacturer:", border=0)

        self.set_xy(self._label_x(LEFT_MARGIN), self._label_y(60))
        self.set_font('Arial', '', 14)
        self.cell(w=0, h=0, align='L', txt=label.manufacturer, border=0)

    def _set_description(self, label):
        self.set_xy(self._label_x(LEFT_MARGIN), self._label_y(71))
        self.set_font('Arial', '', 10)
        self.cell(w=0, h=0, align='L', txt="Description:", border=0)

        self.set_xy(self._label_x(LEFT_MARGIN), self._label_y(74))
        self.set_font('Arial', '', 14)
        self.multi_cell(w=(LABEL_WIDTH - 2*LEFT_MARGIN), h=5, align='L', txt=label.description, border=0)
        self.ln

    def _set_value(self, label):
        self.set_xy(self._label_x(LEFT_MARGIN), self._label_y(100))
        self.set_font('Arial', 'B', 20)
        self.cell(w=0, h=0, align='L', txt=label.value, border=0)

    def _set_image(self, label):
        self.image(label.image, 
                    x = self._label_x(LABEL_WIDTH - IMAGE_WIDTH - LEFT_MARGIN), 
                    y = self._label_y(1), 
                    w = IMAGE_WIDTH, 
                    h = IMAGE_WIDTH, 
                    type = '', 
                    link = '')

if __name__ == "__main__":

    pdf = PDF(orientation='P') #pdf object   

    label_0 = Label("OSCILLATOR", "0001", "Gen_Crystal_32.768kHz_7pF", "AE", "Crystal 3215 32.768kHz 7pF 20ppm", "", "Assets/crystal_2.png")
    label_1 = Label("RESISTOR", "0002", "ERA45405AD", "Samsung", "0805 1% 20ppm", "15.4 kOhms", "Assets/usb_c.png")
    label_2 = Label("CAPACITOR", "0003", "Mfr. ref", "AVX", "Crystal 32kHz", "32 kHz", "Assets/soic8.png")
    label_3 = Label("CONNECTOR", "0004", "Mfr. ref", "AVX", "Crystal 32kHz", "32 kHz", "Assets/sot323_diode.png")
    label_4 = Label("TRANSISTOR", "0005", "Mfr. ref", "AVX", "P-channel MOSFET -30V 2.5A 76mOhms", "32 kHz", "Assets/crystal_2.png")
    label_5 = Label("IC", "0006", "Mfr. ref", "AVX", "Crystal 32kHz", "32 kHz", "Assets/led.png")

    # pdf.add_page()
    pdf.add_label(label_0)
    pdf.add_label(label_1)
    pdf.add_label(label_2)
    pdf.add_label(label_3)
    pdf.add_label(label_4)
    pdf.add_label(label_5)

    # pdf.add_page()
    pdf.add_label(label_0)
    pdf.add_label(label_1)
    pdf.add_label(label_2)
    pdf.add_label(label_3)
    pdf.add_label(label_4)
    pdf.add_label(label_5)
    
    # pdf.titles()
    pdf.output('labels.pdf','F')