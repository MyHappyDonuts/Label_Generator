"""
\file LabelsFromExcel.py
\brief Short brief for this module.
\author Javier Morales

\details Description for this module.
"""

###############
## Import
###############
## Native libraries

## External modules
from LabelGenerator import PDF, Label
from openpyxl import load_workbook

# File paths
stock_path = "Component_Stock.xlsx"

class LabelFromExcel():

    def __init__(self) -> None:
        super().__init__()

        self.pdf = PDF(orientation='P') #pdf object 

        self.wb_stock = load_workbook(stock_path, read_only=True)
        self.ws_stock = self.wb_stock.active

        # Column indexes
        self.col_type = 0
        self.col_mfr_no = 0
        self.col_description = 0
        self.col_value = 0
        self.col_manufacturer = 0
        self.col_icon = 0
        self.col_reference = 0

        self._get_columns()

    # Get important colum indexes
    def _get_columns(self):
        for i in range(1, self.ws_stock.max_column+1):
            header = self.ws_stock.cell(row=1, column=i).value
            match header:
                case "Type":
                    self.col_type = i
                case "Mfr. No":
                    self.col_mfr_no = i
                case "Description":
                    self.col_description = i
                case "Value":
                    self.col_value = i
                case "Manufacturer":
                    self.col_manufacturer = i
                case "Icon":
                    self.col_icon = i
                case "Reference":
                    self.col_reference = i
                case _:
                    pass

    def generate_labels(self):
        for i in range(2, self.ws_stock.max_row+1):
            label_type = self.ws_stock.cell(row=i, column=self.col_type).value
            label_mfr_no = self.ws_stock.cell(row=i, column=self.col_mfr_no).value
            label_description = self.ws_stock.cell(row=i, column=self.col_description).value
            label_value = self.ws_stock.cell(row=i, column=self.col_value).value
            label_manufacturer = self.ws_stock.cell(row=i, column=self.col_manufacturer).value
            label_icon = self.ws_stock.cell(row=i, column=self.col_icon).value
            label_reference = self.ws_stock.cell(row=i, column=self.col_reference).value

            if (label_value == None):
                label_value_str = ""
            else :
                label_value_str = label_value

            label_icon_str = "Assets/" + str(label_icon) + ".png"
            label_reference_str = str(label_reference).zfill(4)

            label = Label(label_type,
                          label_reference_str,
                          label_mfr_no,
                          label_manufacturer,
                          label_description,
                          label_value_str,
                          label_icon_str)

            self.pdf.add_label(label)

        self.pdf.output('labels.pdf','F')

if __name__ == "__main__":

    label_from_excel = LabelFromExcel()

    label_from_excel.generate_labels()